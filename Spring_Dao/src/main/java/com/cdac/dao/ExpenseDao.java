package com.cdac.dao;

import com.cdac.dto.Expense;

public interface ExpenseDao {
	
	void insertExp(Expense exp);
	void deleteExp(int expId);
	void updateExp(Expense exp);
}
