package com.cdac.dao;

import com.cdac.dto.Expense;

public interface ExpenseService {
	void add(Expense exp);
	void remove(int expId);
	void modify(Expense exp);
}
