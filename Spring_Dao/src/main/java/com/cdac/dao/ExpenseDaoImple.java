package com.cdac.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cdac.dto.Expense;

@Repository
public class ExpenseDaoImple implements ExpenseDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void insertExp(Expense exp) {
		String query = "insert into expense(expId,itemName,itemPrice,purchaseDate) values(?,?,?,?)";
		jdbcTemplate.update(query, new Object[] {exp.getExpId(),exp.getItemName(),exp.getItemPrice(),exp.getPurchaseDate()});
		System.out.println("Inserted Successfully");
	}

	public void deleteExp(int expId) {
		String query = "delete from expense where expId = ?";
		jdbcTemplate.update(query, new Object[] {expId});
		System.out.println("Deleted Successfully");
	}

	public void updateExp(Expense exp) {
		String query = "update expense set itemName = ? where expId = ?";
		jdbcTemplate.update(query, new Object[] {exp.getItemName(),exp.getExpId()});
		System.out.println("Updated Successfully");
	}	
}
