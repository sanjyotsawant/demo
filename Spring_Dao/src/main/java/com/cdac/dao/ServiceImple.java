package com.cdac.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdac.dto.Expense;

@Service
public class ServiceImple implements ExpenseService {

	@Autowired
	ExpenseDao expense;
	
	public void add(Expense exp) {
		expense.insertExp(exp);
	}

	public void remove(int expId) {
		expense.deleteExp(expId);
	}

	public void modify(Expense exp) {
		expense.updateExp(exp);
	}	
}
