package com.cdac.dto;

public class Expense {
	private int expId;
	private String itemName;
	private float itemPrice;
	private String purchaseDate;
	public Expense() {
		super();
	}
	public Expense(int expId, String itemName, float itemPrice, String purchaseDate) {
		super();
		this.expId = expId;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.purchaseDate = purchaseDate;
	}
	public int getExpId() {
		return expId;
	}
	public void setExpId(int expId) {
		this.expId = expId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public float getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	@Override
	public String toString() {
		return expId + " " + itemName + " " + itemPrice + " " + purchaseDate;
	}
	
}
