package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cdac.dao.ExpenseService;
import com.cdac.dto.Expense;

public class MainInsert {

	public static void main(String[] args) {

		ApplicationContext appCntx = new ClassPathXmlApplicationContext("cfg.xml");
		ExpenseService es = appCntx.getBean(ExpenseService.class);
		Expense exp = new Expense(102, "PC", 40000.0f, "2020-10-28");
		es.add(exp);
	}

}
